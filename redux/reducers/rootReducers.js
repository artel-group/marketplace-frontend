import categoriesReducer from './categoriesReducers'
import {combineReducers} from 'redux'


const rootReducers = combineReducers({
    categories: categoriesReducer
})

export default rootReducers
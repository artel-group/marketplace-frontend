const API = 'https://marketplace.elliesoft.com'
const VERSION = '1.0.0'


module.exports = {
    env: {
        API_CATEGORIES: {
            "ru": `${API}/ru/${VERSION}/categories`,
            "uz": `${API}/uz/${VERSION}/categories`,
            "en": `${API}/en/${VERSION}/categories`
        },
        API_PRODUCTS_HOME: {
            "ru": `${API}/ru/${VERSION}/home_products`,
            "uz": `${API}/uz/${VERSION}/home_products`,
            "en": `${API}/en/${VERSION}/home_products`
        },
        API_PRODUCT_VIEW: {
            "ru": `${API}/ru/${VERSION}/product`,
            "uz": `${API}/uz/${VERSION}/product`,
            "en": `${API}/en/${VERSION}/product`
        },
        API_PRODUCTS_LIST:{
            "ru": `${API}/ru/${VERSION}/products`,
            "uz": `${API}/uz/${VERSION}/products`,
            "en": `${API}/en/${VERSION}/products`
        },

        GET_CART: {
            "ru": `${API}/ru/${VERSION}/browser`,
            "uz": `${API}/uz/${VERSION}/browser`,
            "en": `${API}/en/${VERSION}/browser`
        },
        ADD_TO_CART: {
            "ru": `${API}/ru/${VERSION}/cart_add`,
            "uz": `${API}/uz/${VERSION}/cart_add`,
            "en": `${API}/en/${VERSION}/cart_add`
        },
        REMOVE_TO_CART: {
            "ru": `${API}/ru/${VERSION}/cart_remove`,
            "uz": `${API}/uz/${VERSION}/cart_remove`,
            "en": `${API}/en/${VERSION}/cart_remove`
        },

        EDIT_TO_CART: {
            "ru": `${API}/ru/${VERSION}/cart_edit`,
            "uz": `${API}/uz/${VERSION}/cart_edit`,
            "en": `${API}/en/${VERSION}/cart_edit`
        },
        NEWS: {
            "ru": `${API}/ru/${VERSION}/blog`,
            "uz": `${API}/uz/${VERSION}/blog`,
            "en": `${API}/en/${VERSION}/blog`
        },
        COMPARES_GET_LIST: {
            "ru": `${API}/ru/${VERSION}/comparison_list`,
            "uz": `${API}/uz/${VERSION}/comparison_list`,
            "en": `${API}/en/${VERSION}/comparison_list`
        },
        COMPARES_ADD: {
            "ru": `${API}/ru/${VERSION}/comparison_add`,
            "uz": `${API}/uz/${VERSION}/comparison_add`,
            "en": `${API}/en/${VERSION}/comparison_add`
        },
        COMPARES_REMOVE: {
            "ru": `${API}/ru/${VERSION}/comparison_remove`,
            "uz": `${API}/uz/${VERSION}/comparison_remove`,
            "en": `${API}/en/${VERSION}/comparison_remove`
        },
        COMPARES_REMOVE_ALL: {
            "ru": `${API}/ru/${VERSION}/comparison_remove_all`,
            "uz": `${API}/uz/${VERSION}/comparison_remove_all`,
            "en": `${API}/en/${VERSION}/comparison_remove_all`
        },
        API: API,
        VERSION: VERSION

    }
}
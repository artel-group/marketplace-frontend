import Slider from "react-slick"
import {useRef, useState} from 'react'


/*----Styles---*/
import classes from '../../../styles/pages-components/home/carousel-banner.module.sass'


/*---Bootstrap-icons----*/
import {PauseCircle, PlayCircle, RecordFill} from "react-bootstrap-icons"

export function CarouselBanner(){

    const g = useRef(null)
    const [stateBtnPause, setState] = useState(true)

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false

    }

    const nextSlide = ()=>{
        g.current.slickNext()
    }
    const prevSlide = ()=>{
        g.current.slickPrev()
    }
    const pauseSlide = ()=>{
        g.current.slickPause()
    }
    const playSlide = ()=>{
        g.current.slickPlay()
    }




    return(
        <div className={`${classes['carousel-wrap']} col-6 pl-0`} id='carousel-home'>
            <div className={`position-absolute ${classes['carousel-controls']}`}>
                <div>
                    <button onClick={prevSlide}>
                        <RecordFill />
                    </button>
                    <button onClick={()=>{
                        if(stateBtnPause){
                            pauseSlide()
                            setState(false)
                        }else{
                            playSlide()
                            setState(true)
                        }
                    }}>{stateBtnPause?<PauseCircle />:<PlayCircle />}</button>
                    <button onClick={nextSlide}>
                        <RecordFill />
                    </button>
                </div>
            </div>
        <Slider {...settings} ref={g}>
            <div>
                <h3>1</h3>
            </div>
            <div>
                <h3>2</h3>
            </div>
            <div>
                <h3>3</h3>
            </div>
            <div>
                <h3>4</h3>
            </div>
            <div>
                <h3>5</h3>
            </div>
            <div>
                <h3>6</h3>
            </div>

        </Slider>
        </div>
    )
}
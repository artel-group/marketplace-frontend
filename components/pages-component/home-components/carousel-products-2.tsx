import Slider from "react-slick"
import Link from 'next/link'
import {useRef, useState} from 'react'
import { useTranslation } from 'react-i18next'


/*----Bootstrap-icons---*/
import {ChevronRight, ChevronLeft} from "react-bootstrap-icons"
import classes from "../../../styles/pages-components/home/carousel-products.module.sass"




/*---Interface----*/
interface IProducts {
    id: number,
    category?: {},
    name: string,
    skus: ISkus[]
}

interface ISkus{
    id: number,
    image_urls: IImage[],
    price: number,
    text_1: string,
    text_2: string,
    slug: string,
    variations?: []
}

interface IImage{
    md: string,
    org: string,
    sm: string,
    xs: string
}

export function CarouselProducts2({products}:{products:IProducts[]}){

    const carousel = useRef(null)
    const {t} = useTranslation()

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: false,
        arrows: false
    }

    const clickNext = ()=>{
        carousel.current.slickNext()
    }

    const clickPrev = ()=>{
        carousel.current.slickPrev()
    }


    return(
        <div className={`${classes['element-carousel']} position-relative`}>
            <p className='mb-0 font-weight-bold'>{t('carousel-products-home.title')}</p>
            <p>{t('carousel-products-home.subTitle')}</p>
            <button className={'position-absolute'} onClick={clickPrev}>
                <ChevronLeft />
            </button>
            <div className='col-11 overflow-hidden mx-auto' id='carousel-product'>
                <Slider {...settings} ref={carousel}>
                    {
                        products.map(item=>{
                            return(
                                <div key={item.id}>
                                    <div className={`${classes['top-info']}`}>
                                        <div className={`${classes['picture-product']}`}>
                                            <img src={item.skus[0].image_urls[0].md} alt={item.name}/>
                                        </div>
                                        <p>{item.name}</p>
                                    </div>
                                    <div className={`${classes['bot-info']}`}>
                                        <p className='mb-0'>{item.skus[0].price} UZS</p>
                                        <p className='mb-0'>{item.skus[0].price} UZS</p>
                                    </div>
                                </div>
                            )
                        })
                    }
                </Slider>

            </div>
            <button className={'position-absolute'} onClick={clickNext}>
                <ChevronRight />
            </button>
        </div>
    )
}
import { useTranslation } from 'react-i18next'
import Link from 'next/link'


/*---Styles----*/
import classes from '../../../styles/pages-components/home/offers-partner.module.sass'

/*---Interfaces----*/

interface IOffers{
    id: number,
    name: string,
    title: string,
    skus: ISku[]

}

interface ISku{
    id: number,
    image_urls: IImage[],
    description: string,
    price: number,
    text_1: string,
    text_2: string,
    slug: string
}

interface IImage {
    md: string,
    org: string,
    sm: string,
    xs: string
}

export function OffersPartners({offers}:{offers:IOffers[]}){

    const {t} = useTranslation()

    return(
        <div className={`${classes['offers-list-wrap']} mt-5`}>
            <p className='font-weight-bold'>{t('offers-partner.title')}</p>
            <div className={`d-flex flex-wrap`}>
                {
                    offers.map((item, index)=>{
                        return(
                            <div className={`${classes['item-offers-partner']} col-lg-4`} key={index}>
                                <div>

                                    <div className={`${classes['product-picture']}`}>
                                        <img src={item.skus[0].image_urls[0].md} alt=""/>
                                    </div>

                                    <div className='mt-3'>
                                        <p>{item.title}</p>
                                        <p>{item.name}</p>
                                        <p>{item.skus[0].description}</p>
                                        <p>
                                            <Link href={'/'}>
                                                <a>Shop the TVs {'>'}</a>
                                            </Link>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}
import Link from 'next/link'



/*---Bootstrap---*/
import {Container} from 'react-bootstrap'

/*----Styles---*/
import classes from '../../styles/main-components/footer.module.sass'


/*---Interface----*/

interface ICategories{
    id: number,
    type: string,
    name: string,
    slug: string,
    children: IChildren[]
}

interface IName {
    ru: string,
    en: string,
    uz: string
}

interface IChildren{
    id: number,
    type: string,
    name: string,
    slug: string,
    children?: IChildren[]
}


interface ICategoriesBot{
    id: number,
    type: string,
    name: IName,
    slug: string,
    children?: IChildren[]
}


export function Footer({categoriesTop, categoriesBot}:{categoriesTop:ICategories[], categoriesBot:ICategoriesBot[]}){

    const renderCategoriesTop = (arr)=>{
        return arr.map((item, index)=>{
            if(item.children.length){
                return(
                    <li className={`${classes['title-list-cat']}`} key={index}>
                        <p className={'mb-0'}>{typeof item.name === "string" ? item.name : item.name['ru']}</p>
                        <ul>{renderCategoriesTop(item.children)}</ul>
                    </li>
                )
            }else{
                <li key={index}><p className={'mb-0'}>{item.name}</p></li>
            }

        })
    }

    return (
        <Container className={classes['footer-wrap']}>
            <ul className={`d-flex ${classes['list-categories-footer']} justify-content-start`}>
                {renderCategoriesTop(categoriesTop)}
            </ul>
            <ul className={`d-flex ${classes['categories-list-bot']}`}>
                {
                    categoriesBot.map((item, index)=>{

                        return(
                            <li key={index}>
                                <Link href={'/'}>
                                    <a>
                                        {typeof item.name === "string" ? item.name : item.name['ru']}
                                    </a>
                                </Link>
                            </li>
                        )
                    })
                }
            </ul>
        </Container>
    )
}
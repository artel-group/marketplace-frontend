import Link from 'next/link'
import {useEffect, useState, useRef} from 'react'


/*---Components-bootstrap---*/
import {DropdownButton, Navbar, NavDropdown, Form, Button, Nav, FormControl} from 'react-bootstrap'


/*----Icons----*/
import {Search, Tags, Globe, GeoAlt, Cart, PersonCircle, Clock} from 'react-bootstrap-icons'

/*---Style---*/
import classes from '../../styles/main-components/header.module.sass'



/*----Interfaces---*/

interface IMenu {
    id: number,
    type: string,
    name:string,
    slug: string,
    children:IChildren[]
}


interface IChildren{
    id: number,
    type: string,
    name:string,
    slug: string,
    children:IChildren[]
}


export function Header({menu}: {menu:IMenu[]}){

    const [showSubMenu, SetShowSubMenu] = useState(false)
    const [subChildren, setSubChildren] = useState([])
    const [subTitleId, setSubTitleId] = useState(0)
    const [subTitleName, setSubTitleName] = useState('')
    const [newObjCategories, setCategories] = useState([])

    const subMenu = useRef(null)
    const collapse = useRef(null)

    useEffect(()=>{
        let newCat = [
            {
                id: 0,
                type: 'menu',
                name:'Продукты',
                slug: '',
                children: menu
            }
        ]

        setCategories(newCat)
    }, [menu])

    const eventClickMobileMenu = (id, name)=>{

        const windowWidth = window.innerWidth

        setSubTitleName(name)
        setSubTitleId(id)

        if(windowWidth<=992){

            const children = findChild(newObjCategories, Number(id))

            if(children){
                if(children.length){
                    SetShowSubMenu(true)
                    setSubChildren(children)
                    setTimeout(()=>{
                        collapse.current.style.height = subMenu.current.offsetHeight + 85 + 'px'
                    }, 10)

                }
            }

        }


    }

    const findChild = (arr, id)=>{
        for(let i=0; i<arr.length; i++){
            if(arr[i] && arr[i].id===id){
                return arr[i].children
            }
            for(let k=0; k<arr[i].children.length; k++){
                if(arr[i].children[k] && arr[i].children[k].id===id){
                    return arr[i].children[k].children
                }
                if(arr[i].children[k].children.length){

                    for(let l=0; l<arr[i].children[k].children.length; l++){
                        if(arr[i].children[k].children[l] && arr[i].children[k].children[l].id===id){
                            return arr[i].children[k].children[l].children
                        }
                    }
                }
            }
        }

        return false

    }


    const renderMenu = ()=>{

        return newObjCategories.map((item, index)=>{
            return(
                <DropdownButton
                    key={item.id}
                    id={`dropdown-button-drop-${item.id}`}
                    title={item.name}
                    className={`${classes['dropdown-link-title']}`}
                    data-name={item.name}
                    data-id={item.id}
                    onClick={()=>eventClickMobileMenu(item.id, item.name)}
                >
                    {renderChild(item.children)}
                </DropdownButton>
            )
        })
    }

    const renderChild = (children)=>{
        return children.map((item)=>{
            if(item.children.length){
                return(
                    <DropdownButton
                        drop='right'
                        key={item.id}
                        id={`dropdown-button-drop--${item.id}`}
                        title={item.name}
                        data-name={item.name}
                        data-id={item.id}
                        className={classes['drop-down-sub']}
                        onClick={()=>eventClickMobileMenu(item.id, item.name)}
                    >
                        {renderChild(item.children)}
                        {
                            item.children.length?(<Link href={`/categories/${item.id}`}>
                                <a>Все категории</a>
                            </Link>):''
                        }
                    </DropdownButton>
                )
            }else{
                return(
                    <Link href={`/products/${item.id}?category_id=${item.id}`} key={item.id}>
                        <a className={classes['nav-link']} key={item.id}>{item.name}</a>
                    </Link>
                )
            }

        })
    }

    return (
        <div>
            <Navbar expand="lg" className={`${classes['nav-bar']}`}>
                <Navbar.Brand href="#home">
                    <div className={classes['icon-brand']}>
                        <img src="/logo-farq.svg" alt=""/>
                    </div>
                </Navbar.Brand>

                <Navbar.Toggle aria-controls="basic-navbar-nav" className={classes['toggle-button']}/>
                <Form inline className={`col-7 ${classes['form-search-nav']}`}>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2 w-100" />
                    <Button className={`position-absolute`}><Search /></Button>
                </Form>
                <Link href={'/'}>
                    <Nav.Link className={`d-lg-none ${classes['md-link']}`}>
                        <Cart />
                        <span>0</span>
                    </Nav.Link>
                </Link>
                <div className='w-100 d-none'>
                    <Navbar.Brand href="#home">
                        <div className={classes['icon-brand']}>
                            <img src="/vercel.svg" alt=""/>
                        </div>
                    </Navbar.Brand>

                    <Navbar.Toggle aria-controls="basic-navbar-nav" className={classes['toggle-button']}/>
                    <Form inline className={`col-7 ${classes['form-search-nav']}`}>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2 w-100" />
                        <Button className={`position-absolute`}><Search /></Button>
                    </Form>
                    <Link href={'/'}>
                        <Nav.Link className={`d-lg-none ${classes['md-link']}`}>
                            <Cart />
                            <span>0</span>
                        </Nav.Link>
                    </Link>
                </div>
                <Navbar.Collapse id="basic-navbar-nav" className='w-100' ref={collapse}>

                    <Nav className={`ml-auto ${classes['top-nav-link']} justify-content-end`}>
                        <Link href={'/'}>
                            <Nav.Link>
                                <GeoAlt />
                                <span>Ташкент</span>
                            </Nav.Link>
                        </Link>
                        <Link href={'/'}>
                            <Nav.Link>
                                <Globe />
                                <span>Ru</span>
                            </Nav.Link>
                        </Link>
                        <Link href={'/'}>
                            <Nav.Link className='d-lg-block d-none'>
                                <Cart />
                                <span>0</span>
                            </Nav.Link>
                        </Link>
                    </Nav>
                    <div className={`d-lg-none ${classes['collapse-navbar']} position-relative`}>
                        <BottomNavbar renderMenu={renderMenu} show={showSubMenu}/>
                        <div className={`${classes['sub-menu-collapse']} ${showSubMenu?`${classes['show']}`:`${classes['hide']}`} position-absolute`} ref={subMenu}>
                            <div className={`text-center`}>
                                <button
                                    data-id={subTitleId}

                                    onClick={(e)=>{
                                        let hasChild = false

                                        for(let k=0; k<menu.length; k++){
                                            for(let i=0; i<menu[k].children.length; i++){
                                                if(menu[k].children[i].id===Number(subTitleId)){

                                                    setSubTitleId(menu[k].id)
                                                    setSubTitleName(menu[k].name['ru'])
                                                    setSubChildren(menu[k].children)
                                                    hasChild = true
                                                    break
                                                }else{
                                                    for(let j=0; j<menu[k].children[i].children.length; j++){
                                                        if(menu[k].children[i].children[j].id===Number(subTitleId)){
                                                            setSubTitleId(menu[k].children[i].id)
                                                            setSubTitleName(menu[k].children[i].name['ru'])
                                                            setSubChildren(menu[k].children[i].children)
                                                            hasChild = true
                                                            break
                                                        }
                                                    }
                                                }
                                            }
                                        }



                                        if(!hasChild) {
                                            SetShowSubMenu(false)
                                            collapse.current.style = ''
                                        }else{
                                            setTimeout(()=>{
                                                collapse.current.style.height = subMenu.current.offsetHeight + 85 + 'px'
                                            }, 10)
                                        }

                                    }}
                                >
                                    {subTitleName}</button>
                            </div>
                            <div>
                                {renderChild(subChildren)}
                            </div>
                        </div>
                    </div>


                </Navbar.Collapse>
            </Navbar>
            <div className='d-lg-block d-none'>
                <BottomNavbar renderMenu={renderMenu} show={showSubMenu}/>
            </div>
        </div>
    )
}


function BottomNavbar({renderMenu, show}:{renderMenu:()=>any, show:boolean}){

    return(
        <Navbar expand="lg" className={`${classes['nav-bar']} flex-lg-row flex-column ${show?'d-none':'d-flex'}`}>
            <Nav className={`mr-auto col-lg-5 col-12 ${classes['top-nav-link']} justify-content-start`}>
                {renderMenu()}
            </Nav>
            <Nav className={`mr-auto col-lg-7 col-12 ${classes['top-nav-link']} ${classes['rgt-link']} justify-content-end `}>
                <div className={'d-flex'}>
                    <PersonCircle className='d-lg-block d-none'/>
                    <DropdownButton
                        id={`dropdown-button-drop-enter`}
                        title={'Вход'}
                    ></DropdownButton>
                </div>
                <div className={'d-flex'}>
                    <Clock className='d-lg-block d-none'/>
                    <DropdownButton
                        id={`dropdown-button-drop-enter`}
                        title={'Недавно просмотренные'}
                    ></DropdownButton>
                </div>
                <div className={'d-flex'}>
                    <svg version="1.1" id="Layer_1"  x="0px" y="0px" className='d-lg-block d-none'
                         width="512px" height="512px" viewBox="0 0 512 512" style={{width: '30px', height: '30px'}}>
                        <g>
                            <path d="M287.8,240c8.8,0,16.1,7.2,16.1,16s-7,16-15.9,16h-64c-8.8,0-16-7.2-16-16s7.2-16,16-16h63 M288,224h-64
                                        c-17.6,0-32,14.4-32,32s14.4,32,32,32h64c17.6,0,32-14.4,32-32S305.6,224,288,224L288,224z"/>
                            <g>
                                <path d="M416,112H96v80h16v208h288V192h16V112z M384,384H128V192h256V384z M400,176H112v-48h288V176z"/>
                            </g>
                        </g>
                    </svg>
                    <DropdownButton
                        id={`dropdown-button-drop-enter`}
                        title={'Статус заказа'}
                    ></DropdownButton>
                </div>
                <div className={'d-flex'}>
                    <svg height="404pt" viewBox="-58 0 404 404.54235" width="404pt" xmlns="http://www.w3.org/2000/svg" className='d-lg-block d-none'>
                        <path d="m277.527344 0h-267.257813c-5.519531 0-10 4.476562-10 10v374.527344c-.007812 7.503906 4.183594 14.378906
                            10.855469 17.808594 6.675781 3.425781 14.707031 2.828124 20.796875-1.550782l111.976563-80.269531 111.980468
                            80.265625c6.09375 4.371094 14.117188 4.964844 20.789063 1.539062 6.667969-3.425781 10.863281-10.296874
                            10.863281-17.792968v-374.527344c0-5.523438-4.480469-10-10.003906-10zm-10 384.523438-117.796875-84.441407c-3.484375-2.496093-8.171875-2.496093-11.652344
                            0l-117.800781 84.445313v-364.527344h247.25zm0 0"/>
                    </svg>
                    <DropdownButton
                        id={`dropdown-button-drop-enter`}
                        title={'Сохраненное'}
                    ></DropdownButton>
                </div>


            </Nav>
        </Navbar>
    )
}
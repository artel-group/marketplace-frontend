import { useTranslation } from 'react-i18next'

/*---Bootstrap-components---*/
import {Container, Form, Button} from "react-bootstrap"


/*---Styles----*/
import classes from '../../styles/main-components/form-subscribe.module.sass'

export function SubscribeForm(){

    const {t} = useTranslation()


    return(
        <div className={`${classes['wrap-subscribe-form']} mt-5`}>
            <Container className={`d-flex justify-content-center`}>
                <div className={`d-flex flex-column justify-content-center`}>
                    <p className={`mb-0`}>{t('subscribe-form.title')}</p>
                </div>
                <div className={`col-lg-6`}>
                    <Form className={`d-flex`}>
                        <Form.Group controlId="formBasicEmail" className={`col-lg-9 mb-0`}>
                            <Form.Control type="email" placeholder={t('subscribe-form.input')} />
                        </Form.Group>
                        <Button type="submit"
                                onClick={(e)=>{
                                    e.preventDefault()
                                }}>
                            {t('subscribe-form.button')}
                        </Button>
                    </Form>
                </div>
            </Container>
        </div>
    )
}
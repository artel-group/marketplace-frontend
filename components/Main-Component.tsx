import {useEffect, useState} from 'react'

/*---Components---*/
import {Header} from './main-components/Header'
import {Footer} from './main-components/Footer'

/*---Virt Db----*/
import {categoriesM, categoriesF, categoriesBot} from '../virtDb/categoriesM'

/*----Redux----*/
import {connect} from 'react-redux'
import {getCategories} from '../redux/actions/actionCategories'

function MainComponent (
    {
        children,
        categories,
        getCategories

    }:
        {
            children: React.ReactElement,
            categories: [],
            getCategories: (value:string)=>any
        }
        ){

    const [categoriesArr, setCategories] = useState([])

    useEffect(()=>{
        getCategories('ru')
    }, [])


    useEffect(()=>{
        setCategories(categories)
    }, [categories])

    return(
        <>
            <Header menu={categoriesArr}/>
                <div>
                    {children}
                </div>
            <Footer categoriesTop={categoriesArr} categoriesBot={categoriesBot}/>
        </>
    )
}

const mapStateToProps = state=>({
    categories: state.categories
})

const mapDispatchToPRops = {
    getCategories

}

export default connect (mapStateToProps, mapDispatchToPRops)(MainComponent)
import  App, { AppProps } from 'next/app'
import Router from 'next/router'
import '../styles/global.sass'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../i18n'

/*---Redux---*/
import {Provider} from 'react-redux'
import withRedux from 'next-redux-wrapper'
import store from '../redux/store'

// function MyApp({ Component, pageProps }) {
//   return <Component {...pageProps} />
// }
//
// export default MyApp


interface IState{
  loading: boolean
}

interface IProps{

}

class MyApp extends App<IProps,IState> {

  state = {
    loading: false
  }

  static async getInitialProps({Component, ctx}) {
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {}
    return {pageProps: pageProps}
  }


  componentDidMount() {
    Router.events.on('routeChangeStart', () => {
      this.setState({loading: true})
    })

    Router.events.on('routeChangeComplete', () => {
      this.setState({loading: false})
    })
  }


  render(){

    const {Component, pageProps} = this.props


    if(this.state.loading){
      return (<h1>Preloader</h1>)
    }else{
      return (
          <Provider store={store}>
            <Component {...pageProps}/>
          </Provider>
      )
    }
  }

}

const makeStore = ()=> store

export default withRedux(makeStore)(MyApp)

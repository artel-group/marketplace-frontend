import MainComponent from '../components/Main-Component'


/*---Bootstrap-components---*/
import {Container} from "react-bootstrap"

/*---Components---*/
import {CarouselBanner} from "../components/pages-component/home-components/carousel-banner"
import {CarouselProducts} from "../components/pages-component/home-components/carousel-products"
import {RecentlyViewed} from "../components/pages-component/home-components/recently-viewed"
import {CarouselProducts2} from "../components/pages-component/home-components/carousel-products-2"
import {OffersPartners} from "../components/pages-component/home-components/offers-partners"
import {SubscribeForm} from '../components/main-components/subscribe-form'

/*----Virt Db----*/
import {offers} from '../virtDb/offers-partner'

function Home({serverData}){

    return(
        <MainComponent>
            <div>
                <Container fluid style={{height: '600px'}} className={`d-flex pt-5`}>
                    <CarouselBanner />
                    <CarouselProducts products={serverData.products} />
                </Container>
                <RecentlyViewed />
                <Container className='pt-3'>
                    <CarouselProducts2 products={serverData.products} />
                    <OffersPartners offers={offers}/>
                </Container>
                <SubscribeForm />
            </div>
        </MainComponent>
    )
}

export async function getServerSideProps(context) {

    const lang = 'ru'

    const res = await fetch(`${process.env.API_PRODUCTS_HOME[lang]}?perPage=12`, {
        credentials: 'include'
    })

    let data = false

    if(res.status===200 || res.status===201){
        data = await res.json()
    }else{
        return{
            props: {
                serverData:{
                    products: null,
                    error: {
                        state: true,
                        status: res.status
                    }
                }
            }

        }
    }

    return {
        props: {serverData:{
                products:data, error: {state:false, status: null}
            }
        }
    }


}


export default Home
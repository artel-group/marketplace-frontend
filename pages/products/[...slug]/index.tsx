import {useRouter} from "next/router"
import {useState, useEffect} from 'react'
import Link from 'next/link'
import { useTranslation } from 'react-i18next'

/*---Components----*/
import MainComponent from "../../../components/Main-Component"
import {Error404} from "../../../components/errors/404"
import {Error500} from "../../../components/errors/500"

/*---Constant---*/
import {SORT_NEW, SORT_ASC, SORT_DESC, SORT_PRICE} from "../../../constants";


/*---Styles---*/
import classes from '../../../styles/pages-components/products/products.module.sass'

/*---Bootstrap---*/
import {Container, Dropdown} from "react-bootstrap"

/*---BootstrapIcons

/*----Redux----*/
import {connect} from 'react-redux'
import {getCategories} from "../../../redux/actions/actionCategories"

/*---Interfaces----*/

interface ICategories {
    id: number,
    children: ICategories[],
    description: string,
    icon_url: {},
    image_urls: IImages,
    name: string,
    parent_id: number
}

interface IImages {
    md: string,
    org: string,
    sm: string,
    xs: string
}

interface IProducts{
    products: {
        total: number,
        data: IData[]
    },
    error: {
        state: boolean,
        status: string | null
    }
}

interface IData{
    id: number,
    meta_description: string,
    meta_keywords: string,
    name: string,
    skus: ISkus[],
    slug: string,
    variations?: []
}

interface ISkus{
    id: number,
    image_urls: IImage[],
    price: number,
    text_1: string,
    text_2: string
}

interface IImage{
    md: string,
    org: string,
    sm: string,
    xs: string
}



function ProductList({categories, serverData}: {categories:ICategories[], serverData:IProducts}){
    const router = useRouter()
    const [title, setTitle] = useState('')
    const [productsList, setProducts] = useState([])
    const {t} = useTranslation()
    const [currentSortValue, setSortValue] = useState('')
    const [totalProducts, setTotalProducts] = useState(0)

    const basePath = `/products/${Array.from(router.query.slug).join('/')}`
    const sortArr = [
        {
            url: `${basePath}?category_id=${router.query.category_id?router.query.category_id:''}&sortBy=${SORT_NEW}&order=${SORT_ASC}`,
            title: t('product-list.dropdown-button-sort-new')
        },
        {
            url: `${basePath}?category_id=${router.query.category_id?router.query.category_id:''}&sortBy=${SORT_PRICE}&order=${SORT_ASC}`,
            title: t('product-list.dropdown-button-sort-price-to-high')
        },
        {
            url: `${basePath}?category_id=${router.query.category_id?router.query.category_id:''}&sortBy=${SORT_PRICE}&order=${SORT_DESC}`,
            title: t('product-list.dropdown-button-sort-price-to-low')
        }
    ]


    useEffect(()=>{
        setSortValue(router.query.sortBy && router.query.order?sortSwitch():t('product-list.dropdown-button-sort-new'))
    }, [])

    useEffect(()=>{
        if(serverData){
            if(!serverData.error.state){
                setProducts(serverData.products.data)
                setTotalProducts(serverData.products.total)
            }
        }else{
            errors()
        }
    }, [serverData])

    const sortSwitch = ()=>{
        switch(router.query.sortBy){
            case SORT_NEW:
                return t('product-list.dropdown-button-sort-new')
            case SORT_PRICE:
                if(router.query.order===SORT_ASC){
                    return t('product-list.dropdown-button-sort-price-to-high')
                }else{
                    return t('product-list.dropdown-button-sort-price-to-low')
                }
            default:
                return t('product-list.dropdown-button-sort-new')

        }
    }

    const errors = ()=>{
        switch(serverData.error.status){
            case '404':
                return <Error404 />
                break
            case '500':
                return <Error404 />
                break
            default:
                return <Error500 />
        }
    }


    useEffect(()=>{
        nestedCat(categories)
    }, [categories])

    const nestedCat = (arr)=>{
        for(let i=0; i<arr.length; i++){
            if(arr[i].id===Number(router.query.slug[router.query.slug.length-1])){
                setTitle(arr[i].name)
            }
            nestedCat(arr[i].children)
        }
    }


    return(
        <MainComponent>
            <Container fluid className={classes['container-products']}>
                <div className={`${classes['bread-crumbs']}`}>
                    <p className='mb-0'>
                        <Link href='/'>
                            <a>Farq</a>
                        </Link>
                    </p>
                    {
                        Array.from(router.query.slug).map((slug, index)=>{

                            if(router.query.slug.length === (index+1)){
                                return(
                                    <p key={index} className='mb-0'>{slug}</p>
                                )
                            }else{
                                return(
                                    <p key={index} className='mb-0'>
                                        <Link href={`/categories/${slug}`}>
                                            <a>{slug}</a>
                                        </Link>
                                    </p>
                                )
                            }
                        })
                    }
                </div>

                <h1 className={`font-weight-bold`}>{title}</h1>

                <div className={`d-flex ${classes['wrap-product-list']}`}>
                    <div className={`col-3 ${classes['filters-products']}`}>
                        <div className={`${classes['sticky-container']}`}>

                        </div>
                    </div>
                    <div className={`col-9 ${classes['products-list-container']}`}>
                        <div className={`${classes['top-content-products']} d-flex justify-content-between`}>
                            <p className={`font-weight-bold mb-0 align-self-center`}>{totalProducts} items</p>
                            <Dropdown className={`${classes['dropdown-filters']} d-flex`}>
                                <p className={`mb-0 align-self-center pr-3`}>
                                    {t('product-list.dropdown-title')}
                                </p>
                                <Dropdown.Toggle variant="success" id="dropdown-basic" >
                                    {currentSortValue}
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    {
                                        sortArr.map((item, index)=>{
                                            return(
                                                <p key={index}>
                                                    <Link href={item.url}>
                                                        <a>{item.title}</a>
                                                    </Link>
                                                </p>
                                            )
                                        })
                                    }
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>
                        {
                            productsList.map(item=>{
                                return(
                                    <div className={`d-flex ${classes['item-product']}`} key={item.id}>
                                        <div className={`${classes['picture-product']} col-lg-3`}>
                                            <Link href={`/`}>
                                                <a>
                                                    <img src={item.skus.length ? item.skus[0].image_urls[0].md : ''} alt={item.name}/>
                                                </a>
                                            </Link>
                                        </div>

                                        <div className={`${classes['info-product']} col-lg-6`}>
                                            <p>
                                                <Link href={`/`}>
                                                    <a>{item.name}</a>
                                                </Link>
                                            </p>
                                            <div className={`${classes['rating-block']}`}>rating</div>
                                            <div className={`${classes['buttons-product']} d-flex`}>
                                                <div className={`d-flex ${classes['button-compare']}`}>
                                                    <svg width="43px" height="43px" viewBox="0 0 43 43" version="1.1">
                                                        <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                                                            <g id="Artboard" fill="#1D2938" fillRule="nonzero">
                                                                <path d="M35.5313212,43 C38.0127183,43 39.8778474,42.3792972 41.1267084,41.1378917 C42.3755695,39.8964862 43,38.0588794 43,35.6250712 L43,7.37492877 C43,4.94112061 42.3755695,3.10351377 41.1267084,1.86210826 C39.8778474,0.620702754 38.0127183,0 35.5313212,0 L7.46867882,0 C5.00360668,0 3.14255885,0.616619183 1.88553531,1.84985755 C0.628511769,3.08309592 0,4.92478632 0,7.37492877 L0,35.6250712 C0,38.0752137 0.628511769,39.9169041 1.88553531,41.1501425 C3.14255885,42.3833808 5.00360668,43 7.46867882,43 L35.5313212,43 Z M35.3599089,38.8102564 L7.64009112,38.8102564 C6.54631739,38.8102564 5.70149962,38.5162393 5.10563781,37.9282051 C4.50977601,37.3401709 4.2118451,36.4662868 4.2118451,35.3065527 L4.2118451,7.69344729 C4.2118451,6.5337132 4.50977601,5.65982906 5.10563781,5.07179487 C5.70149962,4.48376068 6.54631739,4.18974359 7.64009112,4.18974359 L35.3599089,4.18974359 C36.4536826,4.18974359 37.3025816,4.48376068 37.9066059,5.07179487 C38.5106302,5.65982906 38.8126424,6.5337132 38.8126424,7.69344729 L38.8126424,35.3065527 C38.8126424,36.4662868 38.5106302,37.3401709 37.9066059,37.9282051 C37.3025816,38.5162393 36.4536826,38.8102564 35.3599089,38.8102564 Z M17.6309795,21.8797721 C18.055429,21.8797721 18.4104973,21.7409307 18.6961845,21.4632479 C18.9818717,21.1855651 19.1247153,20.8180437 19.1247153,20.3606838 C19.1247153,19.9359924 18.9777904,19.5766382 18.6839408,19.2826211 L16.2596811,16.9059829 L15.3781321,16.1219373 L16.5045558,16.1709402 L31.2215262,16.1709402 C31.6786257,16.1709402 32.050019,16.0239316 32.3357062,15.7299145 C32.6213933,15.4358974 32.7642369,15.0683761 32.7642369,14.6273504 C32.7642369,14.1699905 32.6213933,13.794302 32.3357062,13.5002849 C32.050019,13.2062678 31.6786257,13.0592593 31.2215262,13.0592593 L16.5290433,13.0592593 L15.4026196,13.1082621 L16.2596811,12.3242165 L18.6839408,9.97207977 C18.9777904,9.67806268 19.1247153,9.31870845 19.1247153,8.89401709 C19.1247153,8.43665717 18.9818717,8.06505223 18.6961845,7.77920228 C18.4104973,7.49335233 18.055429,7.35042735 17.6309795,7.35042735 C17.20653,7.35042735 16.8228929,7.51377018 16.4800683,7.84045584 L10.7255125,13.4757835 C10.3990129,13.7698006 10.2357631,14.1495726 10.2357631,14.6150997 C10.2357631,15.0806268 10.3990129,15.468566 10.7255125,15.7789174 L16.4800683,21.4387464 C16.8392179,21.7327635 17.222855,21.8797721 17.6309795,21.8797721 Z M25.393508,35.6495726 C25.7689825,35.6495726 26.1362946,35.494397 26.4954442,35.1840456 L32.2989749,29.4997151 C32.6091496,29.1730294 32.7601557,28.7932574 32.7519932,28.3603989 C32.7438307,27.9275404 32.5928246,27.5559354 32.2989749,27.245584 L26.4954442,21.5612536 C26.1852696,21.2672365 25.8179575,21.1202279 25.393508,21.1202279 C24.9364085,21.1202279 24.5650152,21.2590693 24.279328,21.5367521 C23.9936409,21.8144349 23.8507973,22.1819563 23.8507973,22.6393162 C23.8507973,23.0640076 23.9977221,23.4233618 24.2915718,23.7173789 L26.7648064,26.0695157 L27.6218679,26.8780627 L26.4954442,26.8045584 L11.7539863,26.8045584 C11.3295368,26.8045584 10.9703872,26.951567 10.6765376,27.245584 C10.3826879,27.5396011 10.2357631,27.9152896 10.2357631,28.3726496 C10.2357631,28.8136752 10.3826879,29.1811966 10.6765376,29.4752137 C10.9703872,29.7692308 11.3295368,29.9162393 11.7539863,29.9162393 L26.4954442,29.9162393 L27.6218679,29.8917379 L26.7648064,30.6512821 L24.3160592,33.0279202 C24.0058846,33.3382716 23.8507973,33.6976258 23.8507973,34.1059829 C23.8507973,34.5470085 23.9977221,34.9145299 24.2915718,35.208547 C24.5854214,35.5025641 24.9527335,35.6495726 25.393508,35.6495726 Z" id="Compare"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                    <p className='mb-0'>Compare</p>
                                                </div>

                                                <div className={`d-flex ${classes['button-save']}`}>
                                                    <svg height="404pt" viewBox="-58 0 404 404.54235" width="404pt" xmlns="http://www.w3.org/2000/svg" className='d-lg-block d-none'>
                                                        <path d="m277.527344 0h-267.257813c-5.519531 0-10 4.476562-10 10v374.527344c-.007812 7.503906 4.183594 14.378906
                                                        10.855469 17.808594 6.675781 3.425781 14.707031 2.828124 20.796875-1.550782l111.976563-80.269531 111.980468
                                                        80.265625c6.09375 4.371094 14.117188 4.964844 20.789063 1.539062 6.667969-3.425781 10.863281-10.296874
                                                        10.863281-17.792968v-374.527344c0-5.523438-4.480469-10-10.003906-10zm-10 384.523438-117.796875-84.441407c-3.484375-2.496093-8.171875-2.496093-11.652344
                                                        0l-117.800781 84.445313v-364.527344h247.25zm0 0"/>
                                                    </svg>
                                                    <p className='mb-0'>Save</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div className={`${classes['product-price-cart']} col-lg-3`}>

                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </Container>
        </MainComponent>
    )
}


export async function getServerSideProps(context) {

    const lang = 'ru'
    const {category_id, sortBy, order} = context.query
    const page = context.query.page ? context.query.page : 1


    const res = await fetch(`${process.env.API_PRODUCTS_LIST[lang]}?category_id=${category_id}&perPage=12&sortBy=${sortBy?sortBy:'new'}&order=${order?order:'asc'}&page=${page}`, {
        credentials: 'include'
    })

    let data = false

    if(res.status===200 || res.status===201){
        data = await res.json()
    }else{
        return{
            props: {
                serverData:{
                    products: null,
                    error: {
                        state: true,
                        status: res.status
                    }
                }
            }

        }
    }

    return {
        props: {serverData:{
                products:data, error: {state:false, status: null}
            }
        }
    }


}


const mapStateToProps = state=>({
    categories: state.categories
})

const mapDispatchToPRops = {
    getCategories

}

export default connect (mapStateToProps, mapDispatchToPRops)(ProductList)